# OpenFlexure Microscope Extra 3D Files

This repository is for [OpenFlexure Microscope](https://openflexure.org/projects/microscope/) community contributed 3D files that don't have OpenSCAD source files.
